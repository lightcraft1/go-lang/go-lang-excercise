package main

import "strconv"

// invertMatrix inverts a given matrix represented as a 2D string slice.
// The function transposes the matrix, swapping rows with columns.
// It returns a new 2D string slice representing the inverted matrix.
// If the input matrix is empty, it returns nil.
func invertMatrix(records [][]string) [][]string {
	if len(records) == 0 {
		return nil
	}

	rowCount := len(records)
	colCount := len(records[0])

	inverted := make([][]string, colCount)
	for i := range inverted {
		inverted[i] = make([]string, rowCount)
	}

	for i, row := range records {
		for x, value := range row {
			inverted[x][i] = value
		}
	}
	return inverted
}

// flattenMatrix takes a matrix represented as a 2D string slice and flattens it.
// The function concatenates all rows into a single slice, effectively turning the matrix into a one-dimensional slice.
// It returns this flattened slice.
func flattenMatrix(records [][]string) []string {
	flat := []string{}
	for _, row := range records {
		for _, value := range row {
			flat = append(flat, value)
		}
	}
	return flat
}

// sumMatrix calculates the sum of all elements in a matrix represented as a 2D string slice.
// Each element of the matrix is expected to be a string that can be converted to an integer.
// The function returns the sum as an integer.
// If an element cannot be converted to an integer, it skips that element.
func sumMatrix(records [][]string) int {
	var sum int
	for _, row := range records {
		for _, value := range row {
			intVal, _ := strconv.Atoi(value)
			sum += intVal
		}
	}
	return sum
}

// multiplyMatrix calculates the product of all elements in a matrix represented as a 2D string slice.
// Similar to sumMatrix, it expects each element to be a string convertible to an integer.
// It returns the product as an integer.
// If an element cannot be converted to an integer, it is treated as 1 in the multiplication.
func multiplyMatrix(records [][]string) int {
	prod := 1
	for _, row := range records {
		for _, value := range row {
			intVal, _ := strconv.Atoi(value)
			prod = prod * intVal
		}
	}
	return prod
}
