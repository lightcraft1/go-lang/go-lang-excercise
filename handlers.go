package main

import (
	"fmt"
	"net/http"
	"strings"
)

// echo handles the "/echo" route. It reads a matrix from the request and sends the same matrix back to the client.
// The matrix is formatted as a string with rows delimited by newlines and columns by commas.
func echo(w http.ResponseWriter, r *http.Request) {
	records, err := getRecords(w, r)
	if err != nil {
        http.Error(w, err.Error(), http.StatusBadRequest)
        return
    }

	var response string
	for _, row := range records {
		response = fmt.Sprintf("%s%s\n", response, strings.Join(row, ","))
	}
	fmt.Fprint(w, response)
}

// invert handles the "/invert" route. It reads a matrix from the request, inverts it, and sends the inverted matrix back.
// The inverted matrix is formatted as a string with rows delimited by newlines and columns by commas.
func invert(w http.ResponseWriter, r *http.Request) {
	records, err := getRecords(w, r)
	if err != nil {
        http.Error(w, err.Error(), http.StatusBadRequest)
        return
    }

	var response string
	inverted := invertMatrix(records)

	for _, row := range inverted {
		response = fmt.Sprintf("%s%s\n", response, strings.Join(row, ","))
	}

	fmt.Fprint(w, response)
}

// flatten handles the "/flatten" route. It reads a matrix from the request, flattens it into a single row, and returns it.
// The flattened matrix is formatted as a single string with values separated by commas.
func flatten(w http.ResponseWriter, r *http.Request) {
	records, err := getRecords(w, r)
	if err != nil {
        http.Error(w, err.Error(), http.StatusBadRequest)
        return
    }

	flatten := flattenMatrix(records)

	var response string
	response = fmt.Sprintf("%s%s\n", response, strings.Join(flatten, ","))
	fmt.Fprint(w, response)
}

// sum handles the "/sum" route. It reads a matrix from the request, computes the sum of its elements, and returns the sum.
func sum(w http.ResponseWriter, r *http.Request) {
	records , err := getRecords(w, r)
	if err != nil {
        http.Error(w, err.Error(), http.StatusBadRequest)
        return
    }

	sum := sumMatrix(records)

	var response string
	response = fmt.Sprintf("%s%v\n", response, sum)
	fmt.Fprint(w, response)
}

// multiply handles the "/multiply" route. It reads a matrix from the request, multiplies its elements, and returns the product.
func multiply(w http.ResponseWriter, r *http.Request) {
	records, err := getRecords(w, r)
	if err != nil {
        http.Error(w, err.Error(), http.StatusBadRequest)
        return
    }

	prod := multiplyMatrix(records)

	var response string
	response = fmt.Sprintf("%s%v\n", response, prod)
	fmt.Fprint(w, response)
}
