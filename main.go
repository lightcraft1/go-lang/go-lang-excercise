/*
Package main sets up an HTTP server that provides various matrix operations.
It defines endpoints for echoing, inverting, flattening, summing, and multiplying matrices.

Example curl commands to interact with the server:
- curl -F 'file=@data/matrix.csv' "localhost:8080/echo"
- curl -F 'file=@data/matrix.csv' "localhost:8080/invert"
- curl -F 'file=@data/matrix.csv' "localhost:8080/flatten"
- curl -F 'file=@data/matrix.csv' "localhost:8080/sum"
- curl -F 'file=@data/matrix.csv' "localhost:8080/multiply"
*/
package main

import (
	"net/http"
)

func main() {
	// Sets up HTTP routes. Each route is linked to a function that handles the corresponding matrix operation.
	http.HandleFunc("/echo", echo)         // displays the matrix back to the client
	http.HandleFunc("/invert", invert)     // returns an inverted matrix from the entry
	http.HandleFunc("/flatten", flatten)   // flattens the entry into a sigle  row
	http.HandleFunc("/sum", sum)           // returns the som from the matrix entry
	http.HandleFunc("/multiply", multiply) //returns the product from the entry

	// Starts the HTTP server on port 8080
	http.ListenAndServe(":8080", nil)
}
