package main

import (
	"reflect"
	"testing"
)

func TestInvertMatrix(t *testing.T) {
	tests := []struct {
		name     string
		records  [][]string
		expected [][]string
	}{
		{
			name: "3x3 matrix",
			records: [][]string{
				{"1", "2", "3"},
				{"4", "5", "6"},
				{"7", "8", "9"},
			},
			expected: [][]string{
				{"1", "4", "7"},
				{"2", "5", "8"},
				{"3", "6", "9"},
			},
		},
		{
			name:     "empty matrix",
			records:  [][]string{},
			expected: nil,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := invertMatrix(tt.records)
			if !reflect.DeepEqual(got, tt.expected) {
				t.Errorf("invertMatrix() = %v, want %v", got, tt.expected)
			}
		})
	}
}

func TestFlattenMatrix(t *testing.T) {
	tests := []struct {
		name     string
		records  [][]string
		expected []string
	}{
		{
			name: "3x3 matrix",
			records: [][]string{
				{"1", "2", "3"},
				{"4", "5", "6"},
				{"7", "8", "9"},
			},
			expected: []string{"1", "2", "3", "4", "5", "6", "7", "8", "9"},
		},
		{
			name:     "empty matrix",
			records:  [][]string{},
			expected: []string{},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := flattenMatrix(tt.records)
			if !reflect.DeepEqual(got, tt.expected) {
				t.Errorf("flattenMatrix() = %v, want %v", got, tt.expected)
			}
		})
	}
}

func TestSumMatrix(t *testing.T) {
	tests := []struct {
		name     string
		records  [][]string
		expected int
	}{
		{
			name: "3x3 matrix",
			records: [][]string{
				{"1", "2", "3"},
				{"4", "5", "6"},
				{"7", "8", "9"},
			},
			expected: 45,
		},
		{
			name:     "empty matrix",
			records:  [][]string{},
			expected: 0,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := sumMatrix(tt.records)
			if got != tt.expected {
				t.Errorf("sumMatrix() = %d, want %d", got, tt.expected)
			}
		})
	}
}

func TestMultiplyMatrix(t *testing.T) {
	tests := []struct {
		name     string
		records  [][]string
		expected int
	}{
		{
			name: "2x2 matrix",
			records: [][]string{
				{"1", "2"},
				{"3", "4"},
			},
			expected: 24,
		},
		{
			name:     "empty matrix",
			records:  [][]string{},
			expected: 1,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := multiplyMatrix(tt.records)
			if got != tt.expected {
				t.Errorf("multiplyMatrix() = %d, want %d", got, tt.expected)
			}
		})
	}
}
