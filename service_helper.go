package main

import (
	"encoding/csv"
	"errors"
	"fmt"
	"mime/multipart"
	"net/http"
	"strconv"
)

// validateRecords checks the validity of the matrix represented by a 2D string slice.
// It ensures that the matrix is not empty, is square (equal number of rows and columns),
// and contains only valid integer values.
// Returns an error if any of these validations fail.
func validateRecords(records [][]string) error {
	if len(records) == 0 {
		return errors.New("invalid Entry: There are no records found in the matrix")
	}

	rowCount := len(records)
	colCount := len(records[0])

	if rowCount != colCount {
		return errors.New("invalid Entry: The matrix's row and column does not match")
	}

	for _, row := range records {
		for _, value := range row {
			_, err := strconv.Atoi(value)
			if err != nil {
				return errors.New("invalid Entry: The matrix's input found an invalid value: " + err.Error())
			}
		}
	}
	return nil
}

// getFileFromRequest extracts and returns the file header from a multipart form request.
// It is used to process file uploads in HTTP requests.
// Returns an error if the file cannot be retrieved from the request.
func getFileFromRequest(r *http.Request) (*multipart.FileHeader, error) {
	file, fileHeader, err := r.FormFile("file")
	if err != nil {
		return nil, err
	}
	defer file.Close()
	return fileHeader, nil
}

// getRecords processes an HTTP request to extract and return matrix data from an uploaded CSV file.
// It retrieves the file from the request, reads and parses the CSV data, and validates the matrix.
// Returns the matrix as a 2D string slice or an error if any step fails.
func getRecords(w http.ResponseWriter, r *http.Request) ([][]string, error) {
	fileHeader, err := getFileFromRequest(r)
	if err != nil {
        return nil, fmt.Errorf("error getting file from request: %w", err)
    }


	file, err := fileHeader.Open()
    if err != nil {
        return nil, fmt.Errorf("error opening file: %w", err)
    }
	defer file.Close()

	records, err := csv.NewReader(file).ReadAll()
    if err != nil {
        return nil, fmt.Errorf("error reading CSV: %w", err)
    }

	if err := validateRecords(records); err != nil {
        return nil, err
    }
	return records, nil
}
